//
//  SASFirstViewController.m
//  SASRecognizer
//
//  Created by Akasaka Ryuunosuke on 08/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//
#import "SASKnowledgeBase.h"
#import "SASFirstViewController.h"

@interface SASFirstViewController ()

@end

@implementation SASFirstViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.canvas.drawsModel = false;
    [self.canvas drawBitmap];
}

- (IBAction)recognize:(id)sender {
    self.canvas.drawsModel = self.modelShown.on;
    NSString *result = @"";
    NSDate *startAt = [NSDate date];
    NSMutableArray* sas = [NSMutableArray new];
    CGFloat thr = 7.0f;
    if (self.thrField.text) {
        if ([self.thrField.text floatValue]>0) {
            thr = [self.thrField.text floatValue];
            NSLog(@"Writing new thr %f", thr);
        }
    }
    for (SASLetter*ltr in [SASKnowledgeBase sharedBase].knowledgeBase) {
        CGFloat confidence = [ltr confidenceLevelOfMatchingTo:self.canvas.characterContent threshold:7.0f];
        if (confidence > 0) {
            [sas addObject:@[[NSNumber numberWithFloat:confidence], [@"" stringByAppendingFormat:@"Letter %@, confidence %.2f.",ltr.meaning,confidence], ltr.meaning]];
        }
    }
   NSArray*sis =  [sas sortedArrayWithOptions:0 usingComparator:^NSComparisonResult(NSArray* obj1, NSArray* obj2) {
        if ([obj1[0] floatValue] > [obj2[0] floatValue]) {
            return NSOrderedDescending;
        } else if ([obj1[0] floatValue] < [obj2[0] floatValue]) {
            return NSOrderedAscending;
        }
        return NSOrderedSame;
    }];
    for (NSArray*sus in sis) {
        result = [result stringByAppendingFormat:@"%@\n", sus[1]];
    }
    result = [result stringByAppendingFormat:@"\nDB Size: %i letters", [SASKnowledgeBase sharedBase].knowledgeBase.count];
    result = [result stringByAppendingFormat:@"\nFinished in %.4f sec.", [[NSDate date] timeIntervalSinceDate:startAt]];
    [self.outfield setText:result];
    int sure=0; NSString* letter;
    for (NSArray*l in sis) {
        if ([[NSNumber numberWithFloat:[l[0]floatValue]*100]intValue] > sure) {
            sure = [[NSNumber numberWithFloat:[l[0]floatValue]*100]intValue];
            letter = l[2];
        }
    }
    if(sure>0)[self.tekisuto setText:[NSString stringWithFormat:@"This is %@ (confidence level %i)",letter, sure]];
    else [self.tekisuto setText:@"Sorry, I don't know what this means."];
}
- (IBAction)clear:(id)sender {
    [self.outfield setText:@" "];
    [self.tekisuto setText:@"Please write in the field above"];
    [self.canvas wipeAll];
}
- (IBAction)deoe:(id)sender {
}
@end
