//
//  main.m
//  SASRecognizer
//
//  Created by Akasaka Ryuunosuke on 08/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SASAppDelegate.h"
#import "kbneuesas.h"
#import <objc/runtime.h>
NSArray*UIKeyboardGetSupportedInputModes();
int main(int argc, char * argv[])
{
    @autoreleasepool {
        NSLog(@"Modes %@", UIKeyboardGetSupportedInputModes());
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SASAppDelegate class]));
    }
}
