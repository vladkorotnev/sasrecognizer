//
//  SASLetter.h
//  SASRecognizer
//
//  Created by Akasaka Ryuunosuke on 08/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NSArray SASChar;

@interface SASLetter : NSObject
@property (nonatomic) NSMutableArray *variations;
@property (nonatomic) NSString* meaning;
- (BOOL) compareToInput: (SASChar*) characterContent;
- (BOOL) compareToInput: (SASChar*) characterContent withThreshold:(CGFloat)threshold;
- (CGFloat) confidenceLevelOfMatchingTo:(SASChar*)characterContent threshold:(CGFloat)threshold;
@end
