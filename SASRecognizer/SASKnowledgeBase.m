//
//  SASKnowledgeBase.m
//  SASRecognizer
//
//  Created by Akasaka Ryuunosuke on 08/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import "SASKnowledgeBase.h"
#import "SASLetter.h"
@implementation SASKnowledgeBase
{
    NSMutableArray *kb;
}
- (NSArray*)knowledgeBase { return kb; }

+ (SASKnowledgeBase *)sharedBase
{
	static SASKnowledgeBase *sharedInstance = nil;
	if (sharedInstance == nil)
	{
		sharedInstance = [[self alloc] init];
        sharedInstance->kb = ([[NSUserDefaults standardUserDefaults]objectForKey:@"KB"] ? [sharedInstance _unarchive] :  [NSMutableArray new]);
	}
	return sharedInstance;
}
- (void) _archive {
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:kb];
    [[NSUserDefaults standardUserDefaults]setObject:data forKey:@"KB"];
}
- (NSMutableArray*) _unarchive {
    return [[NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults]objectForKey:@"KB"]]mutableCopy];
}

- (void) memorizeWriting: (SASChar*)writing forMeaning:(NSString*)meaning {
    SASLetter* letter = nil;
    for (SASLetter*ltr in kb) {
        if ([ltr.meaning isEqualToString:meaning]) {
            letter = ltr;
        }
    }
    bool didMemo=false;
    if (!letter) {
        // Memorize
        didMemo=true;
        letter = [[SASLetter alloc]init];
        letter.meaning = meaning;
    }
    [letter.variations addObject:writing];
    if(didMemo)[kb addObject:letter];
    [self _archive];
}

- (void) forget: (NSString*)meaning {
    for (SASLetter*ltr in kb) {
        if ([ltr.meaning isEqualToString:meaning]) {
            
            [kb removeObject:ltr];
            [self _archive];
        }
    }
}
@end
