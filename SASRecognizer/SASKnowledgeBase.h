//
//  SASKnowledgeBase.h
//  SASRecognizer
//
//  Created by Akasaka Ryuunosuke on 08/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SASLetter.h"
@interface SASKnowledgeBase : NSObject
+ (SASKnowledgeBase*) sharedBase;
- (NSArray*)knowledgeBase;
- (void) memorizeWriting: (SASChar*)writing forMeaning:(NSString*)meaning;
- (void) forget: (NSString*)meaning;
@end
