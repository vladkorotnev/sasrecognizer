//
//  SASAppDelegate.h
//  SASRecognizer
//
//  Created by Akasaka Ryuunosuke on 08/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SASAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
