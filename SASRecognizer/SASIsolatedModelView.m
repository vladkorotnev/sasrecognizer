//
//  SASIsolatedModelView.m
//  SASRecognizer
//
//  Created by Akasaka Ryuunosuke on 09/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import "SASIsolatedModelView.h"

@implementation SASIsolatedModelView {
    UIImage *incrementalImage;
}
- (void) drawModel:(SASChar*)model {
    incrementalImage = nil;
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, YES, 0.0);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    if (!incrementalImage) // first time; paint background white
    {
        UIBezierPath *rectpath = [UIBezierPath bezierPathWithRect:self.bounds];
        [self.backgroundColor setFill];
        [rectpath fill];
        // Draw grid
        [[UIColor colorWithWhite:0.8 alpha:1]setStroke];
        CGContextSetLineWidth(ctx, 0.1);
        
        CGContextMoveToPoint(ctx, self.bounds.size.width/3/2, 0);
        CGContextAddLineToPoint(ctx, self.bounds.size.width/3/2, self.bounds.size.height);
        CGContextMoveToPoint(ctx, self.bounds.size.width - self.bounds.size.width/3/2, 0);
        CGContextAddLineToPoint(ctx, self.bounds.size.width - self.bounds.size.width/3/2, self.bounds.size.height);
        
        CGContextMoveToPoint(ctx, 0, self.bounds.size.height/3/2);
        CGContextAddLineToPoint(ctx, self.bounds.size.width, self.bounds.size.height/3/2);
        CGContextMoveToPoint(ctx, 0, self.bounds.size.height - self.bounds.size.height/3/2);
        CGContextAddLineToPoint(ctx, self.bounds.size.width , self.bounds.size.height - self.bounds.size.height/3/2);
        
        CGContextStrokePath(ctx);
    }
    [incrementalImage drawAtPoint:CGPointZero];
    
    CGContextSetLineWidth(ctx, 0.7);
    
    
    
    
    
    [[UIColor blackColor] setStroke];

        
        
        for (NSArray*stroke in model) {
            [(self.tintColor ? self.tintColor : [UIColor greenColor]) setStroke];
            int i;
            CGPoint first;
            first = [stroke[0] CGPointValue];
            CGContextMoveToPoint(ctx, first.x, first.y);
            for (i = 1; i < stroke.count; i++) {
                CGPoint pt = [stroke[i] CGPointValue];
                CGContextAddLineToPoint(ctx, pt.x, pt.y);
                CGContextMoveToPoint(ctx, pt.x, pt.y);
            }
            
            CGContextStrokePath(ctx);
        }
  
    
    incrementalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
     [self setNeedsDisplay];
}
- (void)drawRect:(CGRect)rect
{
    [incrementalImage drawInRect:rect];
}
@end
