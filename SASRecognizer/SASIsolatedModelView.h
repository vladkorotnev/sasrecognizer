//
//  SASIsolatedModelView.h
//  SASRecognizer
//
//  Created by Akasaka Ryuunosuke on 09/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SASLetter.h"

@interface SASIsolatedModelView : UIView
- (void) drawModel:(SASChar*)model;
@end
