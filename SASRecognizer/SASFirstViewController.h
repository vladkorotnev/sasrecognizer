//
//  SASFirstViewController.h
//  SASRecognizer
//
//  Created by Akasaka Ryuunosuke on 08/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SASRecognizerView.h"
#import "SASLetter.h"
@interface SASFirstViewController : UIViewController
- (IBAction)recognize:(id)sender;
@property (weak, nonatomic) IBOutlet SASRecognizerView *canvas;
@property (weak, nonatomic) IBOutlet UITextView *outfield;
- (IBAction)clear:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *tekisuto;
@property (weak, nonatomic) IBOutlet UITextField *thrField;
- (IBAction)deoe:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *modelShown;

@end
