//
//  SASSecondViewController.h
//  SASRecognizer
//
//  Created by Akasaka Ryuunosuke on 08/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SASRecognizerView.h"
@interface SASSecondViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *inputField;
- (IBAction)memorize:(id)sender;
- (IBAction)forget:(id)sender;
- (IBAction)recognize:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *statusBar;
@property (weak, nonatomic) IBOutlet SASRecognizerView *canvas;
- (IBAction)deoe:(id)sender;
- (IBAction)wipe:(id)sender;

@end
