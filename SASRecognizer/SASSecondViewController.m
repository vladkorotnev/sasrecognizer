//
//  SASSecondViewController.m
//  SASRecognizer
//
//  Created by Akasaka Ryuunosuke on 08/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import "SASSecondViewController.h"
#import "SASLetter.h"
#import "SASKnowledgeBase.h"


@implementation SASSecondViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) _stat: (NSString*)st {
    [self.statusBar setText:st];
}
- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.canvas.drawsModel = true;
    [self.canvas drawBitmap];
}
- (IBAction)memorize:(id)sender {
    if (self.inputField.text == nil) {
        [self _stat:@"No input meaning!"];
        return;
    }
    if ([self.inputField.text isEqualToString:@""]) {
        [self _stat:@"No input meaning!"];
        return;
    }
    [self _stat:[NSString stringWithFormat:@"Memorized %@", self.inputField.text]];
    [[SASKnowledgeBase sharedBase]memorizeWriting:self.canvas.characterContent forMeaning:self.inputField.text];
}

- (IBAction)forget:(id)sender {
[self _stat:[NSString stringWithFormat:@"Forgot %@", self.inputField.text]];
  
}

- (IBAction)recognize:(id)sender {
   
}
- (IBAction)deoe:(id)sender {
}

- (IBAction)wipe:(id)sender {
    [self.canvas wipeAll];
}
@end
