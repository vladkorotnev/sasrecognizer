//
//  SASLetter.m
//  SASRecognizer
//
//  Created by Akasaka Ryuunosuke on 08/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import "SASLetter.h"

@implementation SASLetter
- (id)init {
    self = [super init];
    if (self) {
        self.variations = [NSMutableArray new];
        self.meaning = @"";
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super init])) {
        self.variations=[aDecoder decodeObjectForKey:@"vars"];
        self.meaning = [aDecoder decodeObjectForKey:@"mean"];
    }
    return self;
}
-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.variations forKey:@"vars"];
    [aCoder encodeObject:self.meaning forKey:@"mean"];
}
- (BOOL) compareToInput: (SASChar*) characterContent {
    return [self compareToInput:characterContent withThreshold:0.5f];
}

- (BOOL) _comparePoint:(CGPoint) initialPoint toPoint:(CGPoint) secondPoint  threshold:(CGFloat)threshold{
    return [self _distancePoint:initialPoint toPoint:secondPoint] <= threshold;
}
- (float) _distancePoint:(CGPoint) initialPoint toPoint:(CGPoint) secondPoint {
    return sqrtf(( powf( (initialPoint.x - secondPoint.x), 2.0) + powf( (initialPoint.y - secondPoint.y), 2.0))) ;
}

- (CGFloat) confidenceLevelOfMatchingTo:(SASChar *)characterContent threshold:(CGFloat)threshold{
    CGFloat confidenceTotal = 0.0;
    for (SASChar*charVariation in self.variations) {
        CGFloat confidence = 0.0;
        CGPoint firstPoint = [charVariation[0][1] CGPointValue];
        CGPoint firstIn = [characterContent[0][1] CGPointValue];
        
        if ([self _comparePoint:firstPoint toPoint:firstIn threshold:threshold*10] ) {
            confidence += 0.2; NSLog(@"Passes first pt test.");
            NSArray* lastStroke = charVariation[charVariation.count - 1];
            CGPoint lastPoint = [lastStroke[1] CGPointValue];
            NSArray* lastStrokeIn = characterContent[characterContent.count - 1];
            CGPoint lastPointIn = [lastStrokeIn[1] CGPointValue];
            if ([self _comparePoint:lastPointIn toPoint:lastPoint threshold:threshold*10] ) { confidence += 0.2; NSLog(@"Passes last pt test."); }
        }
        if (charVariation.count == characterContent.count) {
            //  confidence += 0.1;
            NSLog(@" seems similar to %@ by count", self.meaning);
            for (int i=0; i < characterContent.count; i++) {
                NSArray* stroke = characterContent[i];
                NSArray* sin = charVariation[i];
                
                if (stroke.count == sin.count) {
                    NSLog(@"Passes stroke point count test");
                    confidence += 0.1;
                    int j;
                    for (j = 0; j < stroke.count; j++) {
                        CGPoint pin = [stroke[j] CGPointValue];
                        CGPoint pt = [sin[j] CGPointValue];
                        if ([self _distancePoint:pin toPoint:pt] <= threshold*10) {
                            NSLog(@"Passes stroke thr test");
                            CGFloat bonus = ( (1.0 - confidence) / charVariation.count ) / stroke.count;
                            if (![self _comparePoint:pin toPoint:pt threshold:threshold*10]) {
                                NSLog(@"No pass distance test");
                                bonus -= [self _distancePoint:pin toPoint:pt] / 10;
                            }
                            if(bonus>0)confidence += bonus;
                            
                        }
                    }
                    
                    
                }
            }
        }
        
        confidenceTotal+= confidence * (1.0 / self.variations.count);
    }
    return confidenceTotal;
}

- (BOOL) compareToInput: (SASChar*) characterContent withThreshold:(CGFloat)threshold {
    CGFloat confidence = [self confidenceLevelOfMatchingTo:characterContent threshold:threshold];
    NSLog(@"COMPARE TO %@: confidence %f",self.meaning,confidence);
    if (confidence < 0.3) {
        return false;
    }
    return true;
}
@end
