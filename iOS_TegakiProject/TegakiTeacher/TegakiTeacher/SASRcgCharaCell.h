//
//  SASRcgCharaCell.h
//  TegakiTeacher
//
//  Created by Akasaka Ryuunosuke on 13/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SASIsolatedModelView.h"
@interface SASRcgCharaCell : UICollectionViewCell
- (void) setChar:(SASChar*)sas;
@end
