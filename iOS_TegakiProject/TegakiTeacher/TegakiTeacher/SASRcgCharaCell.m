//
//  SASRcgCharaCell.m
//  TegakiTeacher
//
//  Created by Akasaka Ryuunosuke on 13/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import "SASRcgCharaCell.h"

@implementation SASRcgCharaCell {
    SASIsolatedModelView*model;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void) setChar:(SASChar*)sas {
    if (!model) {
        model = [[SASIsolatedModelView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [model setBackgroundColor:[UIColor whiteColor]];
        [model setTintColor:[UIColor blackColor]];
        [self addSubview:model];
    }
    [model drawModel:sas];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
