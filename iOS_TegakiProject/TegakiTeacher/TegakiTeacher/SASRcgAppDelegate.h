//
//  SASRcgAppDelegate.h
//  TegakiTeacher
//
//  Created by Akasaka Ryuunosuke on 12/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SASRcgAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
