//
//  SASRcgAddNewWritingViewController.h
//  TegakiTeacher
//
//  Created by Akasaka Ryuunosuke on 15/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SASLetter.h"
@protocol SASAddNewWritingDelegate;
@interface SASRcgAddNewWritingViewController : UIViewController
@property id<SASAddNewWritingDelegate> delegate;
@end

@protocol SASAddNewWritingDelegate <NSObject>

- (void) shouldAddNewWriting: (SASChar*)writing;

@end