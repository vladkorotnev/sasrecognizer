//
//  SASRcgKnowledgeBaseViewController.h
//  TegakiTeacher
//
//  Created by Akasaka Ryuunosuke on 12/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SASKnowledgeBase.h"
#import "SASIsolatedModelView.h"
#import "SASRcgAddNewWritingViewController.h"
@interface SASRcgKnowledgeBaseViewController : UITableViewController <SASAddNewWritingDelegate, UIAlertViewDelegate>

@end
