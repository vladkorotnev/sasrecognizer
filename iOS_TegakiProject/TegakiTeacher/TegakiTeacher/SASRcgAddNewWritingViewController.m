//
//  SASRcgAddNewWritingViewController.m
//  TegakiTeacher
//
//  Created by Akasaka Ryuunosuke on 15/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import "SASRcgAddNewWritingViewController.h"
#import "SASRecognizerView.h"
#import "UIAlertView+Blocks.h"
@interface SASRcgAddNewWritingViewController ()

@end

@implementation SASRcgAddNewWritingViewController {
    SASRecognizerView* sas;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.hidesBottomBarWhenPushed = true;
    self.navigationItem.title = @"Add new writing";
    self.navigationItem.prompt = @"Write in the frame using your finger, then tap Done.";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done)];
    sas = [[SASRecognizerView alloc]initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-self.navigationController.navigationBar.frame.size.height)];
    [sas setBackgroundColor:[UIColor whiteColor]];
    [sas setTintColor:[UIColor blackColor]];
    [sas setDrawsModel:true];
    [self.view addSubview:sas];
}
- (void) done {
    if(self.delegate)
    if ([self.delegate respondsToSelector:@selector(shouldAddNewWriting:)])
        [self.delegate shouldAddNewWriting:sas.characterContent];
    
    [self.navigationController popViewControllerAnimated:true];
    
}
- (void) cancel {
    if (sas.characterContent.count > 0) {
        [[[UIAlertView alloc]initWithTitle:@"Discard current writing?" message:@"Are you sure you want to discard this writing?" cancelButtonItem:[RIButtonItem itemWithLabel:@"Cancel"] otherButtonItems:[RIButtonItem itemWithLabel:@"Discard" action:^{
            [self.navigationController popViewControllerAnimated:true];
        }], nil]show];
    } else [self.navigationController popViewControllerAnimated:true];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
