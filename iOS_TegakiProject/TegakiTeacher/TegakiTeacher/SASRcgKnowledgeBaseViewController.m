//
//  SASRcgKnowledgeBaseViewController.m
//  TegakiTeacher
//
//  Created by Akasaka Ryuunosuke on 12/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import "SASRcgKnowledgeBaseViewController.h"
#import "SASRcgCharacterViewController.h"
@interface SASRcgKnowledgeBaseViewController ()

@end

@implementation SASRcgKnowledgeBaseViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNew)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Export" style:UIBarButtonItemStyleBordered target:self action:@selector(exportBase)];
}
- (void) viewWillAppear:(BOOL)animated {
    [self.tableView reloadData];
}
- (void) exportBase {
    
}
- (void) addNew {
    UIAlertView*snush = [[UIAlertView alloc]initWithTitle:@"Add new symbol" message:@"Type symbol below" delegate:self cancelButtonTitle:@"Discard" otherButtonTitles:@"OK", nil];
    [snush setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [snush show];
}
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != alertView.cancelButtonIndex) {
        NSString*sym = [alertView textFieldAtIndex:0].text;
        SASLetter*sles = [[SASLetter alloc]init];
        sles.meaning = sym;
        SASRcgCharacterViewController *vc = [[SASRcgCharacterViewController alloc]initWithChar: sles InLang:[self baseName] AtIndex:[[SASKnowledgeBase sharedBase]knowledgeBaseForLang:[self baseName]].count];
        
        [self.navigationController pushViewController:vc animated:true];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [[SASKnowledgeBase sharedBase]knowledgeBaseForLang:[self baseName]].count;
}
- (NSString*) baseName { return @"TESTLANG"; }

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     SASLetter*ltr =[[SASKnowledgeBase sharedBase]knowledgeBaseForLang:[self baseName]][indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[ltr meaning]];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:[ltr meaning]];
    }
    
   
    cell.textLabel.text =[ltr meaning];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%lu writing%@", (unsigned long)ltr.variations.count, ltr.variations.count != 1 ? @"s" : @""];
   /* SASIsolatedModelView*modvw = [[SASIsolatedModelView alloc]init];
    modvw.backgroundColor = [UIColor blackColor];
    modvw.tintColor = [UIColor orangeColor];

    cell.accessoryView = modvw;
    [modvw setFrame:CGRectMake(0, 0, 44, 44)];
      if([ltr variations][0])  [modvw drawModel:[ltr variations][0]]; */
    return cell;
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [[SASKnowledgeBase sharedBase]killLetterAtIndex:indexPath.row inLanguage:[self baseName]];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    }
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SASRcgCharacterViewController *vc = [[SASRcgCharacterViewController alloc]initWithChar: [[SASKnowledgeBase sharedBase]knowledgeBaseForLang:[self baseName]][indexPath.row] InLang:[self baseName] AtIndex:indexPath.row];
   
    [self.navigationController pushViewController:vc animated:true];
}
/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
