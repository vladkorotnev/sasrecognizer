//
//  SASRcgCharacterViewController.h
//  TegakiTeacher
//
//  Created by Akasaka Ryuunosuke on 13/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SASLetter.h"
#import "SASRecognizerView.h"
#import "SASRcgAddNewWritingViewController.h";
@interface SASRcgCharacterViewController : UIViewController<SASAddNewWritingDelegate> {
    SASLetter*letter; NSString*lang; NSInteger idx;
}
- (SASRcgCharacterViewController*) initWithChar: (SASLetter*)chara InLang:(NSString*)lingua AtIndex:(NSInteger)idx;
@end
