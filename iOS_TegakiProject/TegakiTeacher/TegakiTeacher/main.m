//
//  main.m
//  TegakiTeacher
//
//  Created by Akasaka Ryuunosuke on 12/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SASRcgAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SASRcgAppDelegate class]));
    }
}
