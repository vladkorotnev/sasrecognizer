//
//  SASRcgCharacterViewController.m
//  TegakiTeacher
//
//  Created by Akasaka Ryuunosuke on 13/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import "SASRcgCharacterViewController.h"
#import "SASRcgCharaCell.h"
#import "UIActionSheet+Blocks.h"
#import "SASKnowledgeBase.h"
#import "SASRcgAddNewWritingViewController.h"
@interface SASRcgCharacterViewController ()

@end

@implementation SASRcgCharacterViewController
{
    UICollectionView *variations;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (SASRcgCharacterViewController*) initWithChar:(SASLetter *)chara InLang:(NSString*)lingua AtIndex:(NSInteger)indx{
    self = [self init];
    if (self) {
        letter = chara;
        lang = lingua.copy;
        idx = indx;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = (letter ? letter.meaning : @"<SASLetter: (null)>");
    self.navigationItem.prompt = @"Tap the + to add new. Tap item to delete.";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(newVar)];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    UICollectionViewFlowLayout* flow = [[UICollectionViewFlowLayout alloc]init];
    [flow setScrollDirection:UICollectionViewScrollDirectionVertical];
    variations = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) collectionViewLayout:flow];
    [variations registerClass:[SASRcgCharaCell class] forCellWithReuseIdentifier:@"Sosa"];
    [variations setBackgroundColor:[UIColor whiteColor]];
    [variations setDelegate:self];
    [variations setDataSource:self];
    [self.view addSubview:variations];
    [variations setContentInset:UIEdgeInsetsMake(self.navigationController.navigationBar.frame.size.height, 10, 0, 10)];
    [flow setItemSize:CGSizeMake(120, 120)];
}

#pragma mark - UICollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return letter.variations.count;
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    SASRcgCharaCell*cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Sosa" forIndexPath:indexPath];
    [cell setChar:letter.variations[indexPath.item]];
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [[[UIActionSheet alloc]initWithTitle:@"Do you really want to delete it?" cancelButtonItem:[RIButtonItem itemWithLabel:@"Not actually"] destructiveButtonItem:[RIButtonItem itemWithLabel:@"Yes I do." action:^{
        
        [letter.variations removeObjectAtIndex:indexPath.item];
        [[SASKnowledgeBase sharedBase]_archive];
        if (letter.variations.count == 0) {
            [[SASKnowledgeBase sharedBase]killLetterAtIndex:idx inLanguage:lang];
            [self.navigationController popViewControllerAnimated:TRUE];
        } else [variations reloadData];
        
    }] otherButtonItems:nil]showInView:self.view];
}



- (void) newVar {
    SASRcgAddNewWritingViewController*vc = [SASRcgAddNewWritingViewController new];
     vc.hidesBottomBarWhenPushed = true;
    [vc setDelegate:self];
    [self.navigationController pushViewController:vc animated:true];
}
- (void) shouldAddNewWriting:(SASChar *)writing {
    [[SASKnowledgeBase sharedBase]memorizeWriting:writing forMeaning:letter.meaning inLanguage:lang];
    [variations reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
