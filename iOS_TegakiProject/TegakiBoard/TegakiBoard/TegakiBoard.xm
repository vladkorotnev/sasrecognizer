// Tegaki Project by AKASAKA

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <Preferences/PSSpecifier.h>
#import <substrate.h>
#import "TegakiLayout.h"

%hook UIKeyboardInputMode

+ (id)keyboardInputModeWithIdentifier:(id)arg1 {
    id o = %orig;
    return o;
}
- (id)primaryLanguage {
    if([TegakiLayout isTegaki:[self identifier]]) return @"Tegaki";
    return %orig;
}
%end

%hook UIKeyboardImpl
/* This is where the magic is! */
+ (Class)layoutClassForInputMode:(NSString*)arg1 keyboardType:(int)arg2 {
    Class sass = %orig;
    if ([TegakiLayout isTegaki: arg1]) {
        return [TegakiLayout class];
    }
    return sass;
}
%end



extern "C" NSArray*UIKeyboardGetSupportedInputModes();
extern "C" NSArray*UIKeyboardGetActiveInputModes();
static NSArray* (*orig_modes)();
NSArray* rep_modes() {
	NSArray* res = [orig_modes() arrayByAddingObjectsFromArray:@[@"TEGAKI", @"TEGAKI_Graffiti"]];
	return res;
}

static NSArray* (*orig_active_modes)();
NSArray* rep_active_modes() {
	NSArray* res = orig_active_modes();
	return res;
}



%ctor {
    %init;
    MSHookFunction(UIKeyboardGetSupportedInputModes, rep_modes, &orig_modes);
    MSHookFunction(UIKeyboardGetActiveInputModes, rep_active_modes, &orig_active_modes);
}