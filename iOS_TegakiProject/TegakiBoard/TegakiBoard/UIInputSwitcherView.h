/*@interface UIInputSwitcherView : NSObject {
    int m_currentInputModeIndex;
    NSMutableArray *m_inputModes;
    BOOL m_keyboardSettingsFromSwitcher;
}

@property(retain) NSArray * inputModes;
@property BOOL keyboardSettingsFromSwitcher;

+ (id)activeInstance;
+ (id)sharedInstance;

- (void)dealloc;
- (int)defaultSelectedIndex;
- (void)didSelectItemAtIndex:(int)arg1;
- (id)inputModes;
- (BOOL)keyboardSettingsFromSwitcher;
- (id)nextInputMode;
- (int)numberOfItems;
- (id)previousInputMode;
- (void)selectInputMode:(id)arg1;
- (void)selectNextInputMode;
- (void)selectPreviousInputMode;
- (void)selectRowForInputMode:(id)arg1;
- (id)selectedInputMode;
- (void)setInputMode:(id)arg1;
- (void)setInputModes:(id)arg1;
- (void)setKeyboardSettingsFromSwitcher:(BOOL)arg1;
- (void)show;
- (id)subtitleForItemAtIndex:(int)arg1;
- (id)titleForItemAtIndex:(int)arg1;

@end*/