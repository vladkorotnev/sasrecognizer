//
//  SASKnowledgeBase.m
//  SASRecognizer
//
//  Created by Akasaka Ryuunosuke on 08/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import "SASKnowledgeBase.h"
#import "SASLetter.h"
@implementation SASKnowledgeBase
{
    NSMutableDictionary *kb;
}
- (NSArray*)knowledgeBaseForLang:(NSString*)lang { return kb[lang]; }
#define CH_DB @"/var/mobile/Library/Application Support/Tegaki_Project/characters.db"
+ (SASKnowledgeBase *)sharedBase
{
	static SASKnowledgeBase *sharedInstance = nil;
	if (sharedInstance == nil)
	{
		sharedInstance = [[self alloc] init];
        NSDictionary*prefs = [NSDictionary dictionaryWithContentsOfFile:CH_DB];
        sharedInstance->kb = ([prefs objectForKey:@"KB"] ? [sharedInstance _unarchive] :  [NSMutableDictionary new]);
	}
	return sharedInstance;
}
- (void) _archive {

    NSLog(@"Trying to write char.db %@", self->kb.allKeys);
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:kb];
    if (![[NSFileManager defaultManager]isWritableFileAtPath:CH_DB]) NSLog(@"Possibly will fail to save file!");
    [@{@"KB":data} writeToFile:CH_DB atomically:true];
}
- (NSMutableDictionary*) _unarchive {
    NSLog(@"Reading char.db");
    NSDictionary* sus = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSDictionary dictionaryWithContentsOfFile:CH_DB]objectForKey:@"KB"]];
    if ([sus isKindOfClass:[NSArray class]]) {
        NSLog(@"Converted char.db from legacy format");
        sus = @{@"TESTLANG": sus};
        self->kb = sus;
        [self _archive];
    }
    return [sus mutableCopy];
}

- (void) memorizeWriting: (SASChar*)writing forMeaning:(NSString*)meaning inLanguage:(NSString*)lang {
    SASLetter* letter = nil;
     NSMutableArray * lingua = (kb[lang] ? [kb[lang]mutableCopy] : [NSMutableArray new]);
    for (SASLetter*ltr in lingua) {
        if ([ltr.meaning isEqualToString:meaning]) {
            letter = ltr;
        }
    }
    bool didMemo=false;
    if (!letter) {
        // Memorize
        didMemo=true;
        letter = [[SASLetter alloc]init];
        letter.meaning = meaning;
    }
    [letter.variations addObject:writing];
    
    if(didMemo) {
       
        [lingua addObject:letter];
    }
    [kb setObject:lingua forKey:lang];
    [self _archive];
}
- (void) killLetterAtIndex: (NSInteger)index inLanguage:(NSString*)language {
    NSMutableArray *kbs = [[self knowledgeBaseForLang:language]mutableCopy];
    if (kbs) {
        NSLog(@"Removing letter %@:%li = %@", language, index, [(SASLetter*) kbs[index] meaning]);
        [kbs removeObjectAtIndex:index];
        [kb setObject:kbs forKey:language];
        [self _archive];
    }
}



+ (NSString*) recognizeChara:(SASChar*)chara WithThreshold:(CGFloat)thr InLang:(NSString*)lang {
    NSString *result = @"";
    if (![[SASKnowledgeBase sharedBase]knowledgeBaseForLang:lang]) {
        return @"";
    }
    NSMutableArray* sas = [NSMutableArray new];
   // thr = MAX(7.0, thr);
    for (SASLetter*ltr in [[SASKnowledgeBase sharedBase]knowledgeBaseForLang:lang]) {
        CGFloat confidence = [ltr confidenceLevelOfMatchingTo:chara threshold:thr];
        if (confidence > 0) {
            [sas addObject:@[[NSNumber numberWithFloat:confidence], [@"" stringByAppendingFormat:@"Letter %@, confidence %.2f.",ltr.meaning,confidence], ltr.meaning]];
        }
    }
    NSArray*sis =  [sas sortedArrayWithOptions:0 usingComparator:^NSComparisonResult(NSArray* obj1, NSArray* obj2) {
        if ([obj1[0] floatValue] > [obj2[0] floatValue]) {
            return NSOrderedDescending;
        } else if ([obj1[0] floatValue] < [obj2[0] floatValue]) {
            return NSOrderedAscending;
        }
        return NSOrderedSame;
    }];
    for (NSArray*sus in sis) {
        result = [result stringByAppendingFormat:@"%@\n", sus[1]];
    }
    int sure=0; NSString* letter=@"";
    for (NSArray*l in sis) {
        NSLog(@"%@", l[1]);
        if ([[NSNumber numberWithFloat:[l[0]floatValue]*100]intValue] > sure) {
            letter= l[2];
            sure =[[NSNumber numberWithFloat:[l[0]floatValue]*100]intValue];
        }
    }
    return letter;
}
@end
