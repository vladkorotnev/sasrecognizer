#line 1 "/Users/tigra/Desktop/Coding/iOS_TegakiProject/TegakiBoard/TegakiBoard/TegakiBoard.xm"


#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <Preferences/PSSpecifier.h>
#import <substrate.h>
#import "TegakiLayout.h"

#include <substrate.h>
@class UIKeyboardImpl; @class UIKeyboardInputMode; 
static id (*_logos_meta_orig$_ungrouped$UIKeyboardInputMode$keyboardInputModeWithIdentifier$)(Class, SEL, id); static id _logos_meta_method$_ungrouped$UIKeyboardInputMode$keyboardInputModeWithIdentifier$(Class, SEL, id); static id (*_logos_orig$_ungrouped$UIKeyboardInputMode$primaryLanguage)(UIKeyboardInputMode*, SEL); static id _logos_method$_ungrouped$UIKeyboardInputMode$primaryLanguage(UIKeyboardInputMode*, SEL); static Class (*_logos_meta_orig$_ungrouped$UIKeyboardImpl$layoutClassForInputMode$keyboardType$)(Class, SEL, NSString*, int); static Class _logos_meta_method$_ungrouped$UIKeyboardImpl$layoutClassForInputMode$keyboardType$(Class, SEL, NSString*, int); 

#line 10 "/Users/tigra/Desktop/Coding/iOS_TegakiProject/TegakiBoard/TegakiBoard/TegakiBoard.xm"


static id _logos_meta_method$_ungrouped$UIKeyboardInputMode$keyboardInputModeWithIdentifier$(Class self, SEL _cmd, id arg1) {
    id o = _logos_meta_orig$_ungrouped$UIKeyboardInputMode$keyboardInputModeWithIdentifier$(self, _cmd, arg1);
    return o;
}
static id _logos_method$_ungrouped$UIKeyboardInputMode$primaryLanguage(UIKeyboardInputMode* self, SEL _cmd) {
    if([TegakiLayout isTegaki:[self identifier]]) return @"Tegaki";
    return _logos_orig$_ungrouped$UIKeyboardInputMode$primaryLanguage(self, _cmd);
}




static Class _logos_meta_method$_ungrouped$UIKeyboardImpl$layoutClassForInputMode$keyboardType$(Class self, SEL _cmd, NSString* arg1, int arg2) {
    Class sass = _logos_meta_orig$_ungrouped$UIKeyboardImpl$layoutClassForInputMode$keyboardType$(self, _cmd, arg1, arg2);
    if ([TegakiLayout isTegaki: arg1]) {
        return [TegakiLayout class];
    }
    return sass;
}




extern "C" NSArray*UIKeyboardGetSupportedInputModes();
extern "C" NSArray*UIKeyboardGetActiveInputModes();
static NSArray* (*orig_modes)();
NSArray* rep_modes() {
	NSArray* res = [orig_modes() arrayByAddingObjectsFromArray:@[@"TEGAKI", @"TEGAKI_Graffiti"]];
	return res;
}

static NSArray* (*orig_active_modes)();
NSArray* rep_active_modes() {
	NSArray* res = orig_active_modes();
	return res;
}



static __attribute__((constructor)) void _logosLocalCtor_9ccceb83() {
    {{Class _logos_class$_ungrouped$UIKeyboardInputMode = objc_getClass("UIKeyboardInputMode"); Class _logos_metaclass$_ungrouped$UIKeyboardInputMode = object_getClass(_logos_class$_ungrouped$UIKeyboardInputMode); MSHookMessageEx(_logos_metaclass$_ungrouped$UIKeyboardInputMode, @selector(keyboardInputModeWithIdentifier:), (IMP)&_logos_meta_method$_ungrouped$UIKeyboardInputMode$keyboardInputModeWithIdentifier$, (IMP*)&_logos_meta_orig$_ungrouped$UIKeyboardInputMode$keyboardInputModeWithIdentifier$);MSHookMessageEx(_logos_class$_ungrouped$UIKeyboardInputMode, @selector(primaryLanguage), (IMP)&_logos_method$_ungrouped$UIKeyboardInputMode$primaryLanguage, (IMP*)&_logos_orig$_ungrouped$UIKeyboardInputMode$primaryLanguage);Class _logos_class$_ungrouped$UIKeyboardImpl = objc_getClass("UIKeyboardImpl"); Class _logos_metaclass$_ungrouped$UIKeyboardImpl = object_getClass(_logos_class$_ungrouped$UIKeyboardImpl); MSHookMessageEx(_logos_metaclass$_ungrouped$UIKeyboardImpl, @selector(layoutClassForInputMode:keyboardType:), (IMP)&_logos_meta_method$_ungrouped$UIKeyboardImpl$layoutClassForInputMode$keyboardType$, (IMP*)&_logos_meta_orig$_ungrouped$UIKeyboardImpl$layoutClassForInputMode$keyboardType$);}}
    MSHookFunction(UIKeyboardGetSupportedInputModes, rep_modes, &orig_modes);
    MSHookFunction(UIKeyboardGetActiveInputModes, rep_active_modes, &orig_active_modes);
}
