//
//  TegakiLayout.m
//  Joj
//
//  Created by Akasaka Ryuunosuke on 12/02/14.
//
//

@interface NSString (JRStringAdditions)

- (BOOL)containsString:(NSString *)string;
- (BOOL)containsString:(NSString *)string
               options:(NSStringCompareOptions)options;

@end

@implementation NSString (JRStringAdditions)

- (BOOL)containsString:(NSString *)string
               options:(NSStringCompareOptions)options {
    NSRange rng = [self rangeOfString:string options:options];
    return rng.location != NSNotFound;
}

- (BOOL)containsString:(NSString *)string {
    return [self containsString:string options:0];
}

@end
#import "TegakiLayout.h"
#import "SASKnowledgeBase.h"

@implementation TegakiLayout
{
    UIImageView* sas;
    UIButton* exit;
    UIButton*test;
    UIButton*bksp;
    UIButton *spc;
    UIButton *enter;
    SASRecognizerView* recognitionView;
    KanaKanjiEigoModeSel *modeSel;
}
+ (bool) isTegaki:(NSString*) st {
    return [[st lowercaseString]isEqualToString:@"tegaki"];
}

- (BOOL)isAlphabeticPlane {return  false; }
UIImage* _UIImageWithName(NSString*name);
- (void)showKeyboardWithInputTraits:(id)arg1 screenTraits:(id)arg2 splitTraits:(id)arg3 {
    [self setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.7]];
    [self setOpaque:true];
    //[[UIButton appearance]setBackgroundColor:[UIColor tr]];
    if(!exit) {
        exit = [[UIButton alloc]initWithFrame:CGRectMake(0, (self.frame.size.height / 4) * 3, 50, 50)];
        [exit setTitle:@"🌐" forState:UIControlStateNormal];
        [exit addTarget:self action:@selector(_nextInput:) forControlEvents:UIControlEventTouchUpInside];
        [exit setUserInteractionEnabled:true];
        [exit setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        [self addSubview:exit];
    }
    if(!test) {
        test = [[UIButton alloc]initWithFrame:CGRectMake(0, (self.frame.size.height / 4) * 2, 50, 50)];
        [test setTitle:@"␣" forState:UIControlStateNormal];
        [test addTarget:self action:@selector(_spc:) forControlEvents:UIControlEventTouchUpInside];
        [test setUserInteractionEnabled:true];
         [test setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        [self addSubview:test];
    }
    if(!bksp) {
        bksp = [[UIButton alloc]initWithFrame:CGRectMake(0, (self.frame.size.height / 4) * 0, 50, 50)];
        [bksp setTitle:@"⌫" forState:UIControlStateNormal];
        [bksp addTarget:self action:@selector(_bksp:) forControlEvents:UIControlEventTouchUpInside];
        [bksp setUserInteractionEnabled:true];
         [test setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        [self addSubview:bksp];
    }
    if(!enter) {
        enter = [[UIButton alloc]initWithFrame:CGRectMake(0, (self.frame.size.height / 4) * 1, 50, 50)];
        [enter setTitle:@"⏎" forState:UIControlStateNormal];
        [enter addTarget:self action:@selector(_return:) forControlEvents:UIControlEventTouchUpInside];
        [enter setUserInteractionEnabled:true]; [enter setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        [self addSubview:enter];
    }
    if (!modeSel) {
        modeSel = [[KanaKanjiEigoModeSel alloc]initForModes:@[@"KANA=あ",@"KANJI=字",@"EIGO=ABC",@"NUMBER=123"] withFrame:CGRectMake(self.frame.size.width-50, 0, 50, self.frame.size.height)];
        modeSel.activeColor = [UIColor lightGrayColor];
        modeSel.inactiveColor = self.backgroundColor   ;
        [self addSubview:modeSel];
        //[modeSel layoutButtons];
        
    }
    if (!recognitionView) {
        recognitionView = [[SASRecognizerView alloc]initWithFrame:CGRectMake(50, 0, self.frame.size.width-100, self.frame.size.height)];
        //[recognitionView setDrawsModel:true];
        [recognitionView setBackgroundColor:[UIColor colorWithWhite:0.8 alpha:0.8]];
        if(recognitionView.tintColor)
            [recognitionView setTintColor:[UIColor blackColor]];
        [recognitionView setSendsDrawEvent:true];
        [recognitionView setDrawEventDelay:1.5];
        [recognitionView setDelegate:self];
       // [recognitionView.layer setCornerRadius:2.0];
        [self addSubview:recognitionView];
    }
    [super showKeyboardWithInputTraits:arg1 screenTraits:arg2 splitTraits:arg3];
   
}
- (void) _bksp:(id)sender {
    [[UIKeyboardImpl activeInstance] handleDelete];
    [recognitionView wipeAll];
}
- (void) _spc:(id)sender {
    [[UIKeyboardImpl activeInstance]insertText:@" "];
}
- (void) _tst:(id)sender {
     [[UIDevice currentDevice] playInputClick];
     [[UIKeyboardImpl activeInstance]insertText:@"Tegaki input test!"];
}
- (void) didFinishDrawingCharacter:(SASChar*)character {
    NSString*rc = [SASKnowledgeBase recognizeChara:character WithThreshold:10.0 InLang:modeSel.activeModeIdentifier];
    if (rc) {
        [[UIKeyboardImpl activeInstance]insertText:rc];
    }
}
- (void) _return:(id)sender {
     [[UIDevice currentDevice] playInputClick];
    [[UIKeyboardImpl activeInstance]insertText:@"\n"];
}
- (void) _nextInput:(id)sender {
    NSLog(@"EXIT PUSH");
    Class sw = NSClassFromString(@"UIInputSwitcherView");

    [[sw sharedInstance]selectNextInputMode];
}
- (id)keyplaneName { return @"Tegakiboard"; }
- (id)keyboardName { return @"Tegakiboard"; }
@end
