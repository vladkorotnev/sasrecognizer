//
//  KanaKanjiEigoModeSel.h
//  TegakiBoard
//
//  Created by Akasaka Ryuunosuke on 12/02/14.
//
//

#import <UIKit/UIKit.h>

@interface KanaKanjiEigoModeSel : UIView
- (KanaKanjiEigoModeSel*) initForModes: (NSArray*) modes withFrame:(CGRect)frame ;
- (void) layoutSubviews;
- (NSString*)activeModeIdentifier;
@property UIColor* inactiveColor;
@property UIColor* activeColor;
@end
