//
//  TegakiLayout.h
//  Joj
//
//  Created by Akasaka Ryuunosuke on 12/02/14.
//
//

#import <UIKit/UIKit.h>

#import "UIKeyboardLayout.h"
#import "SASRecognizerView.h"
#import "UIKeyboardImpl.h"
#import "UIInputSwitcherView.h"
#import "KanaKanjiEigoModeSel.h"
@interface TegakiLayout : UIKeyboardLayout <SASRecognizerDrawEventDelegate>
+ (bool) isTegaki:(NSString*) st;
@end
