//
//  KanaKanjiEigoModeSel.m
//  TegakiBoard
//
//  Created by Akasaka Ryuunosuke on 12/02/14.
//
//

#import "KanaKanjiEigoModeSel.h"

@interface UIColor (sas)
-(UIColor *)readableForegroundColorForThis;
@end

@implementation UIColor  (sas)

-(UIColor *)readableForegroundColorForThis {
    
    const CGFloat* componentColors = CGColorGetComponents(self.CGColor);
    
    CGFloat darknessScore = (((componentColors[0]*255) * 299) + ((componentColors[1]*255) * 587) + ((componentColors[2]*255) * 114)) / 1000;
    
    if (darknessScore >= 125) {
        return [UIColor blackColor];
    }
    
    return [UIColor whiteColor];
}

@end

@implementation KanaKanjiEigoModeSel
{
    NSMutableArray *modes;
    NSMutableArray* buttons;
   int activeButton;
}

- (KanaKanjiEigoModeSel*) initForModes: (NSArray*) modestr withFrame:(CGRect)frame {
    self = [self initWithFrame:frame];
    if (self) {
        buttons = [NSMutableArray new]; modes=[NSMutableArray new];
        for (NSString*modeDescriptor in modestr) {
            NSString*mode = [modeDescriptor componentsSeparatedByString:@"="][0];
            NSString*title = [modeDescriptor componentsSeparatedByString:@"="][1];
            [modes addObject:mode];
            UIButton* btnForMode = [[UIButton alloc]init];
            [btnForMode setTitle:title forState:UIControlStateNormal];
            [btnForMode setBackgroundColor:[UIColor grayColor]];
            [btnForMode setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnForMode setTag:buttons.count];
            [btnForMode addTarget:self action:@selector(buttonChosenAction:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:btnForMode];
            [buttons addObject:btnForMode];
        }
        activeButton = [buttons[0]tag];
        [self layoutSubviews];
        [self updateActiveButtonHighlight];
        NSLog(@"Made new sel mode");
    }
    return self;
}
- (void) layoutSubviews {
    for (int i = 0; i < modes.count; i++) {
        UIButton*bt = buttons[i];
        [bt setFrame:CGRectMake(0, (self.frame.size.height / buttons.count) * i, self.frame.size.width, (self.frame.size.height / buttons.count))];
        NSLog(@"Layering subview %i",i);
    }
}
- (void) updateActiveButtonHighlight {
    for (UIButton*btn in buttons) {
        NSLog(@"UPding");
        if (btn.tag != activeButton) {
            [btn setBackgroundColor:[UIColor grayColor]];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        } else {
            [btn setBackgroundColor:[UIColor whiteColor]];
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }
    }
}
- (void) buttonChosenAction:(UIButton*)sender {
    NSLog(@"Btn chosen %@",sender);
    activeButton = [sender tag];
    [self updateActiveButtonHighlight];
}
- (NSString*)activeModeIdentifier {
    return modes[activeButton];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
