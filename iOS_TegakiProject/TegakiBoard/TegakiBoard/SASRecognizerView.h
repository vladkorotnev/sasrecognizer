//
//  SASRecognizerView.h
//  SASRecognizer
//
//  Created by Akasaka Ryuunosuke on 08/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SASLetter.h"
#import "SASIsolatedModelView.h"
@protocol SASRecognizerDrawEventDelegate;
@interface SASRecognizerView : UIView


- (void) wipeAll;
- (SASChar*) characterContent;
- (void) drawBitmap;
@property bool drawsModel;
@property bool sendsDrawEvent;
@property id<SASRecognizerDrawEventDelegate> delegate;
@property NSInteger drawEventDelay;
@property (weak, nonatomic) IBOutlet SASIsolatedModelView *mvw;
@end

@protocol SASRecognizerDrawEventDelegate <NSObject>

- (void) didFinishDrawingCharacter:(SASChar*)character;

@end