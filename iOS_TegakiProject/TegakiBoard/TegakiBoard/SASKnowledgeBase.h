//
//  SASKnowledgeBase.h
//  SASRecognizer
//
//  Created by Akasaka Ryuunosuke on 08/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SASLetter.h"
@interface SASKnowledgeBase : NSObject
- (void) _archive;
+ (SASKnowledgeBase*) sharedBase;
- (NSArray*)knowledgeBaseForLang:(NSString*)lang;
- (void) memorizeWriting: (SASChar*)writing forMeaning:(NSString*)meaning inLanguage:(NSString*)lang;
+ (NSString*) recognizeChara:(SASChar*)chara WithThreshold:(CGFloat)thr InLang:(NSString*)lang;
- (void) killLetterAtIndex: (NSInteger)index inLanguage:(NSString*)language ;
@end
