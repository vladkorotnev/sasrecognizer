//
//  SASRecognizerView.m
//  SASRecognizer
//
//  Created by Akasaka Ryuunosuke on 08/02/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import "SASRecognizerView.h"
enum DIRECTION {
    NONE, UP, DOWN, LEFT, RIGHT
};
typedef enum DIRECTION DIRECTION;
@implementation SASRecognizerView
{
    UIBezierPath *path;
    UIImage *incrementalImage;
    CGPoint pts[5]; // we now need to keep track of the four points of a Bezier segment and the first control point of the next segment
    uint ctr;
    NSArray* lines;
    NSMutableArray* curStroke;
    NSMutableArray* totalStrokes;
    CGPoint prevPoint;
     DIRECTION prev_horz;  DIRECTION prev_vert;
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self setMultipleTouchEnabled:NO];
      //  [self setBackgroundColor:[UIColor whiteColor]];
        path = [UIBezierPath bezierPath];
        [path setLineWidth:2.0];
        
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setMultipleTouchEnabled:NO];
        path = [UIBezierPath bezierPath];
        [path setLineWidth:2.0];
    }
    return self;
}
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [incrementalImage drawInRect:rect];
    [path stroke];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    ctr = 0;
    UITouch *touch = [touches anyObject];
    pts[0] = [touch locationInView:self];
    curStroke = [NSMutableArray new];
    if(!totalStrokes) totalStrokes = [NSMutableArray new];
    CGPoint start = [touch locationInView:self];
    prevPoint = start;
    prev_vert = NONE; prev_horz = NONE;
    [curStroke addObject:[NSValue valueWithCGPoint:start]];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}
#define  X_THRESHOLD 1.1
#define  Y_THRESHOLD 1.1
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint p = [touch locationInView:self];
    ctr++;
    pts[ctr] = p;
    if (ctr == 4)
    {
        pts[3] = CGPointMake((pts[2].x + pts[4].x)/2.0, (pts[2].y + pts[4].y)/2.0); // move the endpoint to the middle of the line joining the second control point of the first Bezier segment and the first control point of the second Bezier segment
        [path moveToPoint:pts[0]];
        [path addCurveToPoint:pts[3] controlPoint1:pts[1] controlPoint2:pts[2]]; // add a cubic Bezier from pt[0] to pt[3], with control points pt[1] and pt[2]
        [self setNeedsDisplay];
        // replace points and get ready to handle the next segment
        pts[0] = pts[3];
        pts[1] = pts[4];
        ctr = 1;
    }
    
    CGFloat dX = p.x - prevPoint.x;
    CGFloat dY = p.y - prevPoint.y;
  //  NSLog(@"DX: %f, DY: %f",dX,dY);
    bool didChangeDirection = false;
    if (prev_horz == NONE) {
        prev_horz = dX < 0.0 ? LEFT : RIGHT;
    } else if (fabs(dX) > X_THRESHOLD){
        DIRECTION horz = dX < 0.0 ? LEFT : RIGHT;
        if(horz != prev_horz) didChangeDirection=true;
        prev_horz = horz;
    }
    
    if (prev_vert == NONE) {
        prev_vert = dY < 0 ? DOWN : UP;
    } else if(fabs(dY) > Y_THRESHOLD) {
        DIRECTION vert = dY < 0 ? DOWN : UP;
        if(vert != prev_vert) didChangeDirection=true;
        prev_vert = vert;
    }
    if (didChangeDirection) {
        [curStroke addObject:[NSValue valueWithCGPoint:p]];
    }
    
    prevPoint.x= p.x; prevPoint.y = p.y;
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
   
    UITouch *touch = [touches anyObject];
    CGPoint p = [touch locationInView:self];
     [curStroke addObject:[NSValue valueWithCGPoint:p]];
    [totalStrokes addObject:curStroke];
    
    [self drawBitmap];
    if (self.sendsDrawEvent) {
        if ([self.delegate respondsToSelector:@selector(didFinishDrawingCharacter:)]) {
            [self performSelector:@selector(_delecb:) withObject:self.characterContent afterDelay:MAX(0.1, self.drawEventDelay)];
        }
    }
    [self setNeedsDisplay];
    
    [path removeAllPoints];
    ctr = 0;
}
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchesEnded:touches withEvent:event];
}
static UIColor *bgsas;
- (void)drawBitmap
{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, YES, 0.0);
    
     CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    if (!incrementalImage) // first time; paint background white
    {
        UIBezierPath *rectpath = [UIBezierPath bezierPathWithRect:self.bounds];
        if(bgsas)[bgsas setFill];
        else {
            [self.backgroundColor setFill];
            bgsas = [self.backgroundColor copy];
            self.backgroundColor = [UIColor clearColor];
        }
        [rectpath fill];
        
        // Draw grid
        [[UIColor colorWithWhite:1.0 alpha:1.0]setStroke];
        CGContextSetLineWidth(ctx, 0.17);
        
        CGContextMoveToPoint(ctx, self.bounds.size.width/3/2, 0);
        CGContextAddLineToPoint(ctx, self.bounds.size.width/3/2, self.bounds.size.height);
        CGContextMoveToPoint(ctx, self.bounds.size.width - self.bounds.size.width/3/2, 0);
        CGContextAddLineToPoint(ctx, self.bounds.size.width - self.bounds.size.width/3/2, self.bounds.size.height);
        
        CGContextMoveToPoint(ctx, 0, self.bounds.size.height/3/2);
        CGContextAddLineToPoint(ctx, self.bounds.size.width, self.bounds.size.height/3/2);
        CGContextMoveToPoint(ctx, 0, self.bounds.size.height - self.bounds.size.height/3/2);
        CGContextAddLineToPoint(ctx, self.bounds.size.width , self.bounds.size.height - self.bounds.size.height/3/2);
        
        CGContextStrokePath(ctx);
        [[UIColor colorWithWhite:0.0 alpha:0.8]setStroke];
        CGContextSetLineWidth(ctx, 0.1);
        
        CGContextMoveToPoint(ctx, self.bounds.size.width/3/2, 0);
        CGContextAddLineToPoint(ctx, self.bounds.size.width/3/2, self.bounds.size.height);
        CGContextMoveToPoint(ctx, self.bounds.size.width - self.bounds.size.width/3/2, 0);
        CGContextAddLineToPoint(ctx, self.bounds.size.width - self.bounds.size.width/3/2, self.bounds.size.height);
        
        CGContextMoveToPoint(ctx, 0, self.bounds.size.height/3/2);
        CGContextAddLineToPoint(ctx, self.bounds.size.width, self.bounds.size.height/3/2);
        CGContextMoveToPoint(ctx, 0, self.bounds.size.height - self.bounds.size.height/3/2);
        CGContextAddLineToPoint(ctx, self.bounds.size.width , self.bounds.size.height - self.bounds.size.height/3/2);
        
        CGContextStrokePath(ctx);
       
    }
    [incrementalImage drawAtPoint:CGPointZero];
    
    CGContextSetLineWidth(ctx, 0.7);
   
    
    [ (self.tintColor ? self.tintColor : [UIColor blackColor]) setStroke];

    [path stroke];
    
    if(self.drawsModel) {
       
        
        for (NSArray*stroke in totalStrokes) {
            [[UIColor redColor]setStroke];
            int i;
            CGPoint first;
            first = [stroke[0] CGPointValue];
            CGContextMoveToPoint(ctx, first.x, first.y);
            CGContextAddRect(ctx, CGRectMake(first.x-1.5f, first.y-1.5f, 3, 3));
            for (i = 1; i < stroke.count; i++) {
                CGPoint pt = [stroke[i] CGPointValue];
                CGContextAddLineToPoint(ctx, pt.x, pt.y);
                CGContextMoveToPoint(ctx, pt.x, pt.y);
                CGContextAddRect(ctx, CGRectMake(pt.x-1, pt.y-1, 2, 2));
            }
            
            CGContextStrokePath(ctx);
        }
        
        if (self.mvw && totalStrokes.count > 0) {
            NSMutableArray* sas = [self _characterOffsetToBeFromZero:totalStrokes];
            NSMutableArray*sis = [self _character:sas fitIntoBoxOfSide:self.mvw.frame.size.width];
            [self.mvw drawModel:sis];
        }
     /*
        for (NSArray*stroke in self.characterContent) {
            [[UIColor greenColor]setStroke];
            int i;
            CGPoint first;
            first = [stroke[0] CGPointValue];
            CGContextMoveToPoint(ctx, first.x, first.y);
            for (i = 1; i < stroke.count; i++) {
                CGPoint pt = [stroke[i] CGPointValue];
                CGContextAddLineToPoint(ctx, pt.x, pt.y);
                CGContextMoveToPoint(ctx, pt.x, pt.y);
            }
            
            CGContextStrokePath(ctx);
        }*/
    }
      
    incrementalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

}
- (void) _delecb:(SASChar*)ch {
    [self.delegate didFinishDrawingCharacter:[ch copy]];
    [self wipeAll];
}
- (void) wipeAll {
    incrementalImage = nil;
    if(totalStrokes)[totalStrokes removeAllObjects];
    [self drawBitmap];
    [self setNeedsDisplay];
}
- (NSMutableArray*) _characterOffsetToBeFromZero: (NSArray*)character {
    NSMutableArray * relativeCharContent = [NSMutableArray new];
    CGPoint topleftcorner;
    topleftcorner.x = 65534; topleftcorner.y = 65534;
   
    for (NSArray*stroke in character) {
        for (NSValue*pv in stroke) {
            CGPoint pt = [pv CGPointValue];
            if (pt.x < topleftcorner.x)
                topleftcorner.x = pt.x;
            if (pt.y < topleftcorner.y)
                topleftcorner.y = pt.y;
        }
    }
    for (NSArray*stroke in character) { // now offset all
        NSMutableArray *strNew = [NSMutableArray new];
        for (NSValue*pv in stroke) {
            CGPoint pt = [pv CGPointValue];
            pt.x -= topleftcorner.x;
            pt.y -= topleftcorner.y;
            [strNew addObject:[NSValue valueWithCGPoint:pt]];
        }
        [relativeCharContent addObject:strNew];
    }
    return relativeCharContent;
}
- (NSMutableArray*) _character:(NSArray*)character fitIntoBoxOfSide:(CGFloat)side {
    NSMutableArray * relativeCharContent = [NSMutableArray new];
    CGSize biggest;
    
    for (NSArray*stroke in character) {
        for (NSValue*pv in stroke) {
            CGPoint pt = [pv CGPointValue];
            if (pt.x > biggest.width)
                biggest.width = pt.x;
            if(pt.y > biggest.height)
                biggest.height = pt.y;
        }
    }
    CGFloat sizingFactorW = side / biggest.width;
    CGFloat sizingFactorH = side / biggest.height;
    for (NSArray*stroke in character) { // now resize all
        NSMutableArray *strNew = [NSMutableArray new];
        for (NSValue*pv in stroke) {
            CGPoint pt = [pv CGPointValue];
            pt.x *= sizingFactorW;
            pt.y *= sizingFactorH;
            [strNew addObject:[NSValue valueWithCGPoint:pt]];
        }
        [relativeCharContent addObject:strNew];
    }
    return relativeCharContent;
}
- (SASChar*) characterContent {
    if (totalStrokes.count == 0) {
        return totalStrokes;
    }
    NSMutableArray* sas = [self _characterOffsetToBeFromZero:totalStrokes];
    NSMutableArray*sis = [self _character:sas fitIntoBoxOfSide:500];
  //  NSLog(@"AFTER OFFSET: %@",sis);
    return sis;
}
@end
